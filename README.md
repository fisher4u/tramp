# tramp

 **准备：** 

1.通过git或者下载导入idea；

2.(选做)安装 src\main\resources\resources\sql\generator-core-1.0-SNAPSHOT.jar 到maven仓库(用于代码生成，节省开发时间)

采用maven插件形式来生成代码的优点：不需要额外部署，对现有项目代码无入侵，也可以用于其他项目。

3.(选做)安装 src\main\resources\resources\sql\mybatisSqlNav.jar 插件到idea(导航到xml中的sql，提高开发效率)


 **一.框架：springBoot+shiro+mybatis+mysql+vue等** 

已实现的功能：登录+角色权限控制+操作日志，excel导出等

单元测试：采用mockito模拟数据+MockMvc请求http

界面：
![输入图片说明](https://gitee.com/uploads/images/2017/1205/155511_3a1acd24_1651339.png "在这里输入图片标题")

 **二.相当简便的mybatis分页** 

  **控制器再也不用写分页参数了，sql也不需要count语句** 。

例子：
    
    1.浏览器请求：

      localhost:8888/demo/pageCount?currentPage=1&pageSize=10

    2.后台控制器代码：
        @RequestMapping("/pageCount")
        public ResultData pageCount() {
            //指定需要分页的方法,执行了分页及count语句
            this.setPageSqlId("UserDao.getList");
            //指定需要分页的方法,只执行了分页，不执行count语句
            //this.setPageSqlId("UserDao.getList",false)
            List<User> list = userService.getList();
            return successPage(list);
        }

    3.mybatis中的sql代码：

        <select id="getList" resultMap="baseResultMap">
            SELECT * FROM USER
        </select>

    4.返回结果：

    {
	code: 200,
	msg: "操作成功",
	data: [{
		id: "0296d1ca-c935-41d1-991a-8b609cd3c582",
		name: "cjm",
		status: 1
	},
	{
		id: "02a56a67-76df-41f5-bc6f-81edef177ae7",
		name: "cjm",
		status: 1
	},
	{
		id: "04e866b7-8bcd-4f93-977e-0ac5613b50c8",
		name: "cjm",
		status: 1
	}],
	total: 4
    }

    非常方便，此外，如果觉得count效率低，则可以调用重载方法this.setPageSqlId("UserDao.getList",false)
，这样就 **只执行分页，不执行count语句，你可以自定义count语句** 。

 **三.封装excel导出** 

如：一个已经存在的列表分页接口

        @RequestMapping("/pageCount")
        public ResultData pageCount() {
            this.setPageSqlId("UserDao.getList");
            List<User> list = userService.getList();
            return successPage(list);
        }
此时想导出此列表数据到excel，则只需要添加参数：

frameParamExportType=excel

frameParamExportFieldKeys(列表对应的属性)：值为数组

frameParamExportFieldNames(对应属性名称)：值为数组

例：http://localhost:8888/demo/pageCount?frameParamExportType=excel&frameParamExportFieldKeys=["name","status"]&frameParamExportFieldNames=["名称","状态"]

excel内容：![输入图片说明](https://gitee.com/uploads/images/2017/1130/135047_7ca72a88_1651339.png "在这里输入图片标题")

这样便导出excel，复用了列表接口，减少工作量；

 **四.maven插件-代码生成器** 

    自己开发的一个maven插件，扩展性高，方便。详情见博客：(https:](https://gitee.com/uploads/images/2017/1130/135047_7ca72a88_1651339.png "在这里输入图片标题")//my.oschina.net/u/2526698/blog/1556347)
代码生成器执行成功后，提供常用的操作方法（get,delete,update等）：

例：

    @RequestMapping("/get")
    public ResultData get(String id) {
        User user = userService.get(id);
        return success(user);
    }

    @RequestMapping("/insert")
    public ResultData insert(User user) {
        User user1 = userService.insert(user);
        return success(user1);
    }

    @RequestMapping("/delete")
    public ResultData delete(String id) {
        userService.delete(id);
        return success(true);
    }

    @RequestMapping("/update")
    public ResultData update(User user) {
        User result = userService.update(user);
        return success(result);
    }

    @RequestMapping("/updateField")
    public ResultData updateField(String id) {
        User user = userService.get(id);
        Random random = new Random();
        user.setName("updateField_"+random.nextInt(10));
        user.setStatus(random.nextInt(3));
        userService.updateField(user,UserField.update().name().status());
        return success(userService.get(id));
    }

 **五.intellij idea插件** 

    定位到mapper.xml文件
    (1).在service或dao中，当光标在方法上时，按快捷键ctrl+shift+X (或ALT+Inser,选择goToMapper)， 
如果对应xml存在id为此方法名时，可快速定位到方法所在的mapper.xml文件中对应的sql位置。 

    (2).在dao中，如果一个方法在mapper中不存在，则会在mapper中创建一个 id=”方法名”标签;


注：前端套用了 小林攻城狮 / dp-BOOT，表示感谢，相关地址：https://gitee.com/zhocuhenglin/dp-BOOT


qq交流群：461964997