package com.tramp;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.tramp.frame.server.thread.ThreadCacheStore;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * @author chenjm1
 * @since 2017/12/1
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@WebAppConfiguration
public abstract class BaseTest {

	@Before
	public void init() {
		ThreadCacheStore.init();
	}
}
