/**
 * 字典类型管理js
 */

$(function () {
	initialPage();
	getGrid();
});

function initialPage() {
	$(window).resize(function() {
		$('#dataGrid').bootstrapTable('resetView', {height: $(window).height()-54});
	});
}

function getGrid() {
	$('#dataGrid').bootstrapTableEx({
        url: host+'dictType/query?_' + $.now(),
		height: $(window).height()-54,
		queryParams: function(params){
            return {
                name:vm.keyword,
                pageSize: params.pageSize,
                currentPage:params.pageNumber
			}
		},
		columns: [{
            radio:true
		}, {
			field : "name",
			title : "名称",
			width : "200px"
		}, {
            field : "value",
            title : "值",
            width : "200px"
        }, {
            field : "value",
            title : "操作",
            width : "200px",
            formatter : function(value, row, index) {
            	var html = "";
                html+='<a class="btn btn-default" onclick="edit(\''+row.id+'\');"><i class="fa fa-pencil-square-o"></i></a>';
                html+='&nbsp;<a class="btn btn-default" onclick="delet(\''+row.id+'\');"><i class="fa fa-trash-o"></i></a>';
                html+='&nbsp;<a class="btn btn-default" onclick="delet(\''+row.id+'\');"><i class="fa fa-eye"></i></a>';
                return html;
            }
        }]
	})
}

function edit(id) {
	console.info("edit:"+id);
    if(checkedRow(id)){
        dialogOpen({
            title: '编辑',
            url: host+'dictType/add.html?_' + $.now(),
            width: '420px',
            height: '350px',
            success: function(iframeId){
                top.frames[iframeId].vm.role.id = id;
                top.frames[iframeId].vm.setForm();
            },
            yes: function(iframeId){
                top.frames[iframeId].vm.acceptClick();
            }
        });
    }
}

function delet(id) {
    console.info("delet:"+id);
    if(checkedArray(id)){
        $.deleteForm({
            url: host+'dictType/delete?_' + $.now(),
            param: {"id":id},
            success: function(data) {
                vm.load();
            }
        });
    }
}

var vm = new Vue({
	el:'#dpLTE',
	data: {
		keyword: null
	},
	methods : {
		load: function() {
			$('#dataGrid').bootstrapTable('refresh');
		},
		save: function() {
			dialogOpen({
				title: '新增',
                url: host+'dictType/add.html?_' + $.now(),
				width: '520px',
				height: '350px',
				yes : function(iframeId) {
					top.frames[iframeId].vm.acceptClick();
				},
			});
		},
		edit: function() {
			var ck = $('#dataGrid').bootstrapTable('getSelections');
			if(checkedRow(ck)){
				dialogOpen({
					title: '编辑',
                    url: host+'dictType/add.html?_' + $.now(),
					width: '420px',
					height: '350px',
					success: function(iframeId){
						top.frames[iframeId].vm.role.id = ck[0].id;
						top.frames[iframeId].vm.setForm();
					},
					yes: function(iframeId){
						top.frames[iframeId].vm.acceptClick();
					}
				});
			}
		},
		remove: function() {
			var ck = $('#dataGrid').bootstrapTable('getSelections'), ids = [];	
			if(checkedArray(ck)){
				$.each(ck, function(idx, item){
					ids[idx] = item.roleId;
				});
				$.deleteForm({
                    url: host+'role/delete?_' + $.now(),
                    param: {"id":ck[0].id},
			    	success: function(data) {
			    		vm.load();
			    	}
				});
			}
		}
	}
})