/**
 * 新增-角色管理js
 */
var vm = new Vue({
	el:'#dpLTE',
	data: {
		role: {
			orgId: 0,
			orgName: null
		}
	},
	methods : {
        setForm: function() {
            $.ajax({
                url : host+'role/info?_' + $.now(),
                data : {"id":vm.role.id},
                success : function(data) {
                    if (data.code != '200') {
                        dialogAlert(data.msg, 'error');
                    } else if (data.code == '200') {
                        vm.role = data.data;
                    }
                },
                error : function(XMLHttpRequest, textStatus, errorThrown) {
                    dialogLoading(false);
                    if(XMLHttpRequest.responseJSON.code == 401){
                        toUrl('login.html');
                    } else if(textStatus=="error"){
                        dialogMsg("请求超时，请稍候重试...", "error");
                    } else {
                        dialogMsg(errorThrown, 'error');
                    }
                }
            });

        },
		acceptClick: function() {
			if (!$('#form').Validform()) {
		        return false;
		    }
		    $.SaveForm({
                url: host+'role/save?_' + $.now(),
		    	param: vm.role,
		    	success: function(data) {
		    		$.currentIframe().vm.load();
		    	}
		    });
		}
	}
})
