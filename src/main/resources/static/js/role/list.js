/**
 * 角色管理js
 */

$(function () {
	initialPage();
	getGrid();
});

function initialPage() {
	$(window).resize(function() {
		$('#dataGrid').bootstrapTable('resetView', {height: $(window).height()-54});
	});
}

function getGrid() {
	$('#dataGrid').bootstrapTableEx({
        url: host+'role/query?_' + $.now(),
		height: $(window).height()-54,
		queryParams: function(params){
            return {
                name:vm.keyword,
                pageSize: params.pageSize,
                currentPage:params.pageNumber
			}
		},
		columns: [{
            radio:true
		}, {
			field : "name",
			title : "角色名称",
			width : "200px"
		}, {
			field : "createTime",
			title : "创建时间"
		}]
	})
}

var vm = new Vue({
	el:'#dpLTE',
	data: {
		keyword: null
	},
	methods : {
		load: function() {
			$('#dataGrid').bootstrapTable('refresh');
		},
		save: function() {
			dialogOpen({
				title: '新增角色',
                url: host+'role/add.html?_' + $.now(),
				width: '420px',
				height: '350px',
				yes : function(iframeId) {
					top.frames[iframeId].vm.acceptClick();
				},
			});
		},
		edit: function() {
			var ck = $('#dataGrid').bootstrapTable('getSelections');
			if(checkedRow(ck)){
				dialogOpen({
					title: '编辑角色',
                    url: host+'role/add.html?_' + $.now(),
					width: '420px',
					height: '350px',
					success: function(iframeId){
						top.frames[iframeId].vm.role.id = ck[0].id;
						top.frames[iframeId].vm.setForm();
					},
					yes: function(iframeId){
						top.frames[iframeId].vm.acceptClick();
					}
				});
			}
		},
		remove: function() {
			var ck = $('#dataGrid').bootstrapTable('getSelections'), ids = [];	
			if(checkedArray(ck)){
				$.each(ck, function(idx, item){
					ids[idx] = item.roleId;
				});
				$.deleteForm({
                    url: host+'role/remove?_' + $.now(),
                    param: {"id":ck[0].id},
			    	success: function(data) {
			    		vm.load();
			    	}
				});
			}
		},
		authorizeOpt: function(){
			var ck = $('#dataGrid').bootstrapTable('getSelections');
			if(checkedRow(ck)){
				dialogOpen({
					title: '操作权限',
					url: host+'role/menuAuth?_' + $.now(),
					scroll : true,
					width: "300px",
					height: "450px",
					success: function(iframeId){
						top.frames[iframeId].vm.role.id = ck[0].id;
						top.frames[iframeId].vm.setForm();
					},
					yes : function(iframeId) {
						top.frames[iframeId].vm.acceptClick();
					}
				})
			}
		},
		authorizeData: function(){
			var ck = $('#dataGrid').bootstrapTable('getSelections');
			if(checkedRow(ck)){
				dialogOpen({
					title: '数据权限',
					url: 'base/role/data.html?_' + $.now(),
					scroll : true,
					width: "300px",
					height: "450px",
					success: function(iframeId){
						top.frames[iframeId].vm.role.roleId = ck[0].roleId;
						top.frames[iframeId].vm.setForm();
					},
					yes : function(iframeId) {
						top.frames[iframeId].vm.acceptClick();
					}
				})
			}
		},
	}
})