package com.tramp.controller;

import com.tramp.basic.entity.DictType;
import com.tramp.basic.service.DictTypeService;
import com.tramp.frame.server.base.BaseController;
import com.tramp.frame.server.base.ResultData;
import com.tramp.frame.server.exception.GenericException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 字典类型控制器
 * Created by chen on 2017/12/2.
 */
@Controller
@RequestMapping("/dictType/")
public class DictTypeController extends BaseController {

    @Autowired
    private DictTypeService dictTypeService;

    /**
     * 列表页
     *
     * @return
     */
    @RequestMapping(value = "list")
    public String list() {

        return "dictType/list";
    }

    /**
     * 列表数据
     *
     * @return
     */
    @RequestMapping("/query")
    @ResponseBody
    public ResultData query() {
        this.setPageSqlId("DictTypeBaseDao.listByEntity");
        List<DictType> dictTypeList = dictTypeService.listByEntity(new DictType());
        return successPage(dictTypeList);
    }

    /**
     * 添加页
     *
     * @return
     */
    @RequestMapping(value = "add")
    public String add() {

        return "dictType/add";
    }

    /**
     * 保存
     *
     * @return
     */
    @RequestMapping("/save")
    @ResponseBody
    public ResultData save(@RequestBody DictType dictType) {
        dictTypeService.insert(dictType);
        return success(dictType);
    }

    /**
     * 删除
     *
     * @return
     */
    @RequestMapping("/delete")
    @ResponseBody
    public ResultData delete(String id) {
        if (StringUtils.isBlank(id)) {
            throw new GenericException("参数错误");
        }
        dictTypeService.delete(id);
        return success();
    }
}
