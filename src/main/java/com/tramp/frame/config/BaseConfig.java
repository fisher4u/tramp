package com.tramp.frame.config;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * Created by chen on 2017/10/22.
 */
@Component
@Configuration
@PropertySource(value = "classpath:baseConfig.properties")
@ConfigurationProperties
public class BaseConfig {

	@Value("${uploadPath}")
	private String uploadPath;

	@Value("${mybatisPage.start}")
	private String start;

	@Value("${mybatisPage.limit}")
	private String limit;

	public String getStart() {
		return StringUtils.isBlank(start) ? "start" : start;
	}

	public void setStart(String start) {
		this.start = start;
	}

	public String getLimit() {
		return StringUtils.isBlank(limit) ? "limit" : limit;
	}

	public void setLimit(String limit) {
		this.limit = limit;
	}

	public String getUploadPath() {
		return uploadPath;
	}

	public void setUploadPath(String uploadPath) {
		this.uploadPath = uploadPath;
	}
}
