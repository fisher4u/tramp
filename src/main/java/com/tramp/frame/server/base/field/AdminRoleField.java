package com.tramp.frame.server.base.field;

/**
* 管理员角色表字段
* @author mbg
* @since 2017-12-05 11:23:36
*/
public final class AdminRoleField extends BaseField {

    private AdminRoleField() {
        super();
    }

    public static AdminRoleField update() {
        return new AdminRoleField();
    }

    public AdminRoleField adminId() {
        this.addField("adminId");
        return this;
    }

    public AdminRoleField roleId() {
        this.addField("roleId");
        return this;
    }

}
