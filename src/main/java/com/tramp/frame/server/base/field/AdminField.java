package com.tramp.frame.server.base.field;

/**
* 管理人员表字段
* @author mbg
* @since 2017-12-05 11:23:36
*/
public final class AdminField extends BaseField {

    private AdminField() {
        super();
    }

    public static AdminField update() {
        return new AdminField();
    }

    public AdminField username() {
        this.addField("username");
        return this;
    }

    public AdminField password() {
        this.addField("password");
        return this;
    }

    public AdminField status() {
        this.addField("status");
        return this;
    }

    public AdminField name() {
        this.addField("name");
        return this;
    }

    public AdminField mobile() {
        this.addField("mobile");
        return this;
    }

    public AdminField remark() {
        this.addField("remark");
        return this;
    }

    public AdminField updateTime() {
        this.addField("updateTime");
        return this;
    }

    public AdminField updateUser() {
        this.addField("updateUser");
        return this;
    }

}
