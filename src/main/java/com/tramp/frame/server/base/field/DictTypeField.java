package com.tramp.frame.server.base.field;

/**
* 字典类型表字段
* @author mbg
* @since 2017-12-05 11:23:36
*/
public final class DictTypeField extends BaseField {

    private DictTypeField() {
        super();
    }

    public static DictTypeField update() {
        return new DictTypeField();
    }

    public DictTypeField value() {
        this.addField("value");
        return this;
    }

    public DictTypeField name() {
        this.addField("name");
        return this;
    }

    public DictTypeField status() {
        this.addField("status");
        return this;
    }

    public DictTypeField ordering() {
        this.addField("ordering");
        return this;
    }

    public DictTypeField updateTime() {
        this.addField("updateTime");
        return this;
    }

    public DictTypeField updateUser() {
        this.addField("updateUser");
        return this;
    }

}
