package com.tramp.frame.server.base.field;

/**
* 角色表字段
* @author mbg
* @since 2017-12-05 11:23:36
*/
public final class RoleField extends BaseField {

    private RoleField() {
        super();
    }

    public static RoleField update() {
        return new RoleField();
    }

    public RoleField parentId() {
        this.addField("parentId");
        return this;
    }

    public RoleField name() {
        this.addField("name");
        return this;
    }

    public RoleField updateTime() {
        this.addField("updateTime");
        return this;
    }

    public RoleField updateUser() {
        this.addField("updateUser");
        return this;
    }

}
