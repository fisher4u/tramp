package com.tramp.frame.server.base.dao;

import com.tramp.frame.server.base.dao.BaseDao;

/**
* 管理员角色数据层
* @author liulanghan
* @since 2017-11-26 11:12:03
*/
public interface AdminRoleBaseDao extends BaseDao {

}
