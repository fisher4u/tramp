package com.tramp.frame.server.base.dao;

import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 * BASE DAO
 */
public interface BaseDao<T> {

    T get(Object id);

    int delete(Collection<String> ids);

    int insert(T record);

    int insertBatch(Collection<T> record);

    int update(T record);

    int updateSelective(T record);

    //int updateField(Map record);

    int updateBatch(Collection<T> record);

    List<T> listByEntity(T record);

    int updateField(T entity, Map<String, String> fields);

}
