/** 
 * EnumParam.java 2016-11-24
 *
 * Copyright 2000-2016 by ChinanetCenter Corporation.
 *
 * All rights reserved.
 *
 * This software is the confidential and proprietary information of
 * ChinanetCenter Corporation ("Confidential Information").  You
 * shall not disclose such Confidential Information and shall use
 * it only in accordance with the terms of the license agreement
 * you entered into with ChinanetCenter.
 *
*/

package com.tramp.frame.server.base;

public interface EnumParam {
	String getCode();

	String getNameCn();

	String getNameEn();
}
