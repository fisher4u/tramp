
package com.tramp.frame.server.thread;

import java.util.HashMap;

import com.tramp.frame.server.exception.GenericException;

public final class ThreadCacheStore {

	private static ThreadLocal<HashMap<String, Object>> threadCacheMap = new ThreadLocal<HashMap<String, Object>>() {
		@Override
		public HashMap<String, Object> initialValue() {
			return new HashMap<>();
		}
	};

	private ThreadCacheStore() {

	}

	public static void init() {
		if (null != threadCacheMap) {
			threadCacheMap.remove();
		}
	}

	public static void destory() {
		if (null != threadCacheMap) {
			threadCacheMap.remove();
		}
	}

	protected static void setCache(String key, Object value) {
		getThreadCacheMap().put(key, value);
	}

	protected static Object getCache(String key) {
		return getThreadCacheMap().get(key);
	}

	/**************************** private *********************************/

	private static HashMap<String, Object> getThreadCacheMap() {
		if (null == threadCacheMap) {
			throw new GenericException("ThreadCacheStore has not been properly initialized,threadCacheMap is null!");
		}
		if (null == threadCacheMap.get()) {
			throw new GenericException("ThreadCacheStore has not been properly initialized,threadCacheMap.get() is null");
		}
		return threadCacheMap.get();
	}

}
