package com.tramp.basic.dao;

import com.tramp.basic.entity.Menu;
import com.tramp.frame.server.base.dao.BaseDao;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * 菜单数据层
 *
 * @author liulanghan
 * @since 2017-11-04 17:42:44
 */
public interface MenuDao {

    List<Menu> getRootMenus();

    List<Menu> getMenusByParent(@Param("parentId") String parentId);

    List<String> listIdByRoleId(@Param("roleId") String roleId);

    List<String> getAuthMenuIds(@Param("adminId") String adminId);

    List<Menu> listParentId(@Param("parentId") String parentId);
}
