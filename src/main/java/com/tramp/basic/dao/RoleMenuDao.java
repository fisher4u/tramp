package com.tramp.basic.dao;

import com.tramp.frame.server.base.dao.BaseDao;
import com.tramp.basic.entity.RoleMenu;
import org.apache.ibatis.annotations.Param;

/**
* 角色菜单数据层
* @author liulanghan
* @since 2017-11-24 11:26:06
*/
public interface RoleMenuDao {

    void deleteByRoleId(@Param("roleId") String roleId);

    void deleteByMenuId(@Param("menuId") String menuId);
}
