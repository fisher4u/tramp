package com.tramp.basic.service;

import com.tramp.frame.server.base.dao.RoleMenuBaseDao;
import com.tramp.basic.dao.RoleMenuDao;
import com.tramp.frame.server.base.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tramp.basic.entity.RoleMenu;
import org.springframework.transaction.annotation.Transactional;


/**
* 角色菜单业务逻辑层
* @author liulanghan
* @since 2017-11-24 11:26:06
*/
@Service
@Transactional
public class RoleMenuService extends BaseService<RoleMenu> {

    @Autowired
    private RoleMenuDao roleMenuDao;

    public RoleMenuService(RoleMenuBaseDao roleMenuDao) {
        super(roleMenuDao);
    }

    public void deleteByRoleId(String roleId) {
        roleMenuDao.deleteByRoleId(roleId);

    }

    public void deleteByMenuId(String menuId) {
        roleMenuDao.deleteByMenuId(menuId);
    }
}
