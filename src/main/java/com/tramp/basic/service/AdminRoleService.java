package com.tramp.basic.service;

import com.tramp.frame.server.base.dao.AdminRoleBaseDao;
import com.tramp.basic.dao.AdminRoleDao;
import com.tramp.frame.server.base.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tramp.basic.entity.AdminRole;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


/**
* 管理员角色业务逻辑层
* @author liulanghan
* @since 2017-11-23 18:17:31
*/
@Service
@Transactional
public class AdminRoleService extends BaseService<AdminRole> {

    @Autowired
    private AdminRoleDao adminRoleDao;

    public AdminRoleService(AdminRoleBaseDao adminRoleDao) {
        super(adminRoleDao);
    }

    public List<String> getRoleIdList(String adminId) {
        return adminRoleDao.getRoleIdList(adminId);
    }
}
