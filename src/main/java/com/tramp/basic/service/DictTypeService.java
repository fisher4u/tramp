package com.tramp.basic.service;

import com.tramp.frame.server.base.dao.DictTypeBaseDao;
import com.tramp.basic.dao.DictTypeDao;
import com.tramp.frame.server.base.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tramp.basic.entity.DictType;
import org.springframework.transaction.annotation.Transactional;


/**
* 字典类型业务逻辑层
* @author liulanghan
* @since 2017-12-02 12:59:31
*/
@Service
@Transactional
public class DictTypeService extends BaseService<DictType> {

    @Autowired
    private DictTypeDao dictTypeDao;

    public DictTypeService(DictTypeBaseDao dictTypeDao) {
        super(dictTypeDao);
    }

}
