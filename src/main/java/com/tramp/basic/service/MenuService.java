package com.tramp.basic.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.collect.Lists;
import com.tramp.basic.dao.MenuDao;
import com.tramp.basic.entity.Menu;
import com.tramp.basic.vo.MenuVO;
import com.tramp.frame.server.base.BaseService;
import com.tramp.frame.server.base.dao.MenuBaseDao;

/**
* 菜单业务逻辑层
* @author liulanghan
* @since 2017-11-04 17:42:44
*/
@Service
@Transactional
public class MenuService extends BaseService<Menu> {

	@Autowired
	private MenuDao menuDao;

	public MenuService(MenuBaseDao menuDao) {
		super(menuDao);
	}

	public List<Menu> getRootMenus() {
		return menuDao.getRootMenus();
	}

	public List<Menu> getMenusByParent(String parentId) {
		return menuDao.getMenusByParent(parentId);
	}

	public List<String> listIdByRoleId(String roleId) {
		return menuDao.listIdByRoleId(roleId);
	}

	public List<String> getAuthMenuIds(String adminId) {
		return menuDao.getAuthMenuIds(adminId);
	}

	public List<MenuVO> getAuthMenus(String adminId) {
		List<String> authMenuIds = this.getAuthMenuIds(adminId);
		//查询根菜单列表
		List<Menu> menuList = listParentId("0", authMenuIds);
		//递归获取子菜单
		List<MenuVO> treeList = getMenuTreeList(menuList, authMenuIds);
		return treeList;
	}

	/**
	 * 递归
	 */
	private List<MenuVO> getMenuTreeList(List<Menu> menuList, List<String> menuIdList) {
		List<MenuVO> menuVOList = Lists.newArrayList();

		for (Menu menu : menuList) {
			MenuVO menuVO = new MenuVO();
			BeanUtils.copyProperties(menu, menuVO);

			menuVO.setMenuList(getMenuTreeList(listParentId(menu.getId(), menuIdList), menuIdList));

			menuVOList.add(menuVO);
		}
		return menuVOList;
	}

	public List<Menu> listParentId(String parentId, List<String> menuIdList) {
		List<Menu> menuList = menuDao.listParentId(parentId);
		if (menuIdList == null) {
			return menuList;
		}

		List<Menu> adminMenuList = new ArrayList<>();
		for (Menu menu : menuList) {
			if (menuIdList.contains(menu.getId())) {
				adminMenuList.add(menu);
			}
		}
		return adminMenuList;
	}
}
