package com.tramp.basic.service;

import com.tramp.frame.server.base.dao.DictDataBaseDao;
import com.tramp.basic.dao.DictDataDao;
import com.tramp.frame.server.base.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tramp.basic.entity.DictData;
import org.springframework.transaction.annotation.Transactional;


/**
* 字典数据业务逻辑层
* @author liulanghan
* @since 2017-12-02 12:59:31
*/
@Service
@Transactional
public class DictDataService extends BaseService<DictData> {

    @Autowired
    private DictDataDao dictDataDao;

    public DictDataService(DictDataBaseDao dictDataDao) {
        super(dictDataDao);
    }

}
