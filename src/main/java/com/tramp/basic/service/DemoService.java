package com.tramp.basic.service;

import com.tramp.frame.server.base.dao.DemoBaseDao;
import com.tramp.basic.dao.DemoDao;
import com.tramp.frame.server.base.BaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.tramp.basic.entity.Demo;
import org.springframework.transaction.annotation.Transactional;


/**
* 业务逻辑层
* @author liulanghan
* @since 2017-12-01 10:53:03
*/
@Service
@Transactional
public class DemoService extends BaseService<Demo> {

    @Autowired
    private DemoDao demoDao;

    public DemoService(DemoBaseDao demoDao) {
        super(demoDao);
    }

}
