package com.tramp.basic.vo;

import java.util.List;

/**
 * Created by chen on 2017/11/11.
 */
public class RoleVO {
    private String id;
    private String name;
    private List<String> menuIdList;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getMenuIdList() {
        return menuIdList;
    }

    public void setMenuIdList(List<String> menuIdList) {
        this.menuIdList = menuIdList;
    }
}
