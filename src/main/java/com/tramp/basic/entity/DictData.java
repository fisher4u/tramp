package com.tramp.basic.entity;

import com.tramp.frame.server.base.BaseEntity;
import com.tramp.frame.server.base.Recordable;
import java.util.Date;


/**
* 字典数据
* @author mbg
* @since 2017-12-05 11:23:36
*/
public class DictData implements Recordable,  BaseEntity{

    private String id; //主键
    private String typeId; //类型id
    private String value; //值
    private String name; //名称
    private String ext; //扩展
    private Boolean status; //状体(1启用,0禁用)
    private Integer ordering; //排序
    private Date createTime; //创建时间
    private String createUser; //
    private Date updateTime; //
    private String updateUser; //

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTypeId() {
        return typeId;
    }

    public void setTypeId(String typeId) {
        this.typeId = typeId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getExt() {
        return ext;
    }

    public void setExt(String ext) {
        this.ext = ext;
    }

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public Integer getOrdering() {
        return ordering;
    }

    public void setOrdering(Integer ordering) {
        this.ordering = ordering;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreateUser() {
        return createUser;
    }

    public void setCreateUser(String createUser) {
        this.createUser = createUser;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getUpdateUser() {
        return updateUser;
    }

    public void setUpdateUser(String updateUser) {
        this.updateUser = updateUser;
    }


}
