package com.tramp.basic.enums;

/**
 * @author chenjm1
 * @since 2017/11/21
 */
public enum AdminStatusEnum {

	STATUS_OK(true, "启用"), STATUS_UNABLE(false, "禁用");

	AdminStatusEnum(Boolean code, String display) {

		this.code = code;
		this.display = display;
	}

	private Boolean code;
	private String display;

	public Boolean getCode() {
		return code;
	}

	public void setCode(Boolean code) {
		this.code = code;
	}

	public String getDisplay() {
		return display;
	}

	public void setDisplay(String display) {
		this.display = display;
	}

}
