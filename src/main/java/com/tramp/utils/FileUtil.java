package com.tramp.utils;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;


/**文件辅助类
 */
public class FileUtil {
	 /**保存文件
	 * @param stream
	 * @param path
	 * @param filename
	 * @throws IOException
	 */
	public static void saveFileFromInputStream(InputStream stream,String path,String filename) throws IOException
    {      
		/***
		 * 如果文件夹不存在则创建文件夹
		 */
		File fls = new File(path);
		if(!fls.exists()){
			fls.mkdirs();
		}
        FileOutputStream fs=new FileOutputStream( path + "/"+ filename);
        byte[] buffer =new byte[1024*1024];
        int bytesum = 0;
        int byteread = 0; 
        while ((byteread=stream.read(buffer))!=-1)
        {
           bytesum+=byteread;
           fs.write(buffer,0,byteread);
           fs.flush();
        } 
        fs.close();
        stream.close();      
    }  
	/**
	* 创建目录
	* 文件所在目录
	* 
	* @throws Exception
	*/
	public static void createDir(String filePath){
		File fileDir = new File(filePath);
		try {
			if (!fileDir.exists()) {
				/**
				 * =================目录不存在，正在新建目录。。。。。。。。。
				 */
				System.out.println("Code_Wizard:目录不存在，正在新建目录。。。。。。。。。" + (new Date()));
				fileDir.mkdirs();
			}else{
				/**
				 * =================目录已经存在
				 */
				System.out.println("Code_Wizard:目录已经存在，不执行新建目录。" + (new Date()));
			}
			/**
			 * =================目录信息
			 */
			System.out.println("Code_Wizard:" + filePath);
		} catch (Exception ex) {
			throw new RuntimeException("创建文件目录出错：" + ex.getMessage());
		}
	}
	
	/**
	 * 删除文件
	 * @param filePathAndName
	 * @return
	 */
    public static boolean delFile(String filePathAndName) {  
        boolean bea = false;  
        try {           
            File myDelFile = new File(filePathAndName);  
            if (myDelFile.exists()) {  
                myDelFile.delete();  
                bea = true;  
            } 
        } catch (Exception e) { 
        	e.printStackTrace();
        }  
        return bea;  
    }  
  
    /**
     * 删除文件夹及包含的文件
     * @param folderPath
     */
    public static boolean delFolder(String folderPath) {
    	boolean bea = false;  
        try {  
            delAllFile(folderPath); // 删除完里面所有内容              
            File myFilePath = new File(folderPath);
            myFilePath.delete(); // 删除空文件夹 
            bea=true;
        } catch (Exception e) {  
          e.printStackTrace();
        } 
        return bea;
    }  
  
   /**
    *  删除指定文件夹下所有文件
    * @param path
    * @return
    */
    public static boolean delAllFile(String path) {  
        boolean bea = false;  
        File file = new File(path);  
        if (!file.exists()) {  
            return bea;  
        }  
        if (!file.isDirectory()) {  
            return bea;  
        }  
        String[] tempList = file.list();  
        File temp = null;  
        for (int i = 0; i < tempList.length; i++) {  
            if (path.endsWith(File.separator)) {  
                temp = new File(path + tempList[i]);  
            } else {  
                temp = new File(path + File.separator + tempList[i]);  
            }  
            if (temp.isFile()) {  
                temp.delete();  
            }  
            if (temp.isDirectory()) {  
                delAllFile(path + "/" + tempList[i]);// 先删除文件夹里面的文件  
                delFolder(path + "/" + tempList[i]);// 再删除空文件  
                bea = true;  
            }  
        }  
        return bea;  
    }  
    
    public static void main(String[] args) {
		String path="D:/1/2";
		String pathAndFileName="D:/1/2/1.txt";		
		//delFile(pathAndFileName);
		delFolder(path);
	}
}
